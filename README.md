# Bash-Stash

A collection of console based bash scripts I've written at various times. It may include scripts written in other languages. To check out this repo you must select a branch you can do this via the following command .

git clone -b branch URL 

# Console 

These are daily use type console scripts.

# OpenWrt 

These are designed to be used with OpenWrt for various tasks.

Please see the read me in each branch for more info.